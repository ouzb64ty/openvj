# openvj

Openvj est un logiciel de veejay open source et libre via le protocole MIDI.
Il est possible de mixer des GIFS, des vidéos et des images sans GPU.
Pour simplifier la recherche du contenu, le script _generate.js_ existe, il utilise l'API de GIPHY et télécharge les GIFS en fonction d'une limite, d'un offset, d'un nom de preset et d'un mot-clef.
Le script se charge ensuite de générer la configuration TOML adéquate dans le dossier _presets_.
Pour le moment Openvj fonctionne avec un nano Kontrol2 mais l'idée est de développer un moteur VJ permettant à tous les utilisateurs de configurer leurs propres contrôleurs MIDI pour mixer de la vidéo et des caméras IP en live avec une interface graphique de configuration et d'interaction.

## Installation

- `npm install`
- `mkdir presets`

Si un problème avec le module _midi_ intervient :

- `npm rebuild`
ou
- `./node_modules/.bin/electron-rebuild`

### Hello World

- Utiliser Linux
- Brancher le nanoKontrol2
- `node generate.js 8 0 hello hello`
- `node index.js presets`

## TODO

- Interface electron pour le soft :
	- Track
	- Pause/Play
	- Preset suivant/précédent
	- Moniteur pour la projection
	- Mapping
	- Chat
	- Systeme de configuration MIDI
- Interface electron pour le generateur de presets :
	- Integrer Tumblr et Facebook
	- Partage via IPFS
- API Rest
- BDD de presets partage
- Documentation
- Evenements
- Filtres et effets

## A savoir

Un preset est un ensemble de GIFS/Videos/Cameras/Images, soit 8 chaines (le nombre de chaines) pour le nano Kontrol2.
Il contient un fichier de configuration TOML pour attribuer aux différentes chaines un média.
Exemples dans le dossier _demo__.
N'hésitez pas à me contacter pour en savoir plus.
J'utilise ce logiciel pour faire mon VJ from scratch en NodeJS via un vidéo-projecteur de 4000 lumens.

## Auteur

ouzb65ty <ouzb65ty@protonmail.ch>