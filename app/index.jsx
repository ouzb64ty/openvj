import React from 'react'
import ReactDOM from 'react-dom'
import io from 'socket.io-client';

class Player extends React.Component {
    constructor() {
        super();
        this.state = {
            preset: {}
        }
        var socket = io('http://localhost:8000');

        socket.on('change-preset', (preset_) => {
            this.setState({
                preset: preset_
            });
            console.log(this.state.preset);
        });
    }

    render() {
        return (
            <div id="player">
                <img class="master-img" src={"http://127.0.0.1:8000/" + this.state.preset.title + "/" + this.state.preset.cover}></img>
                <h2 class="master-title">{ this.state.preset.title }</h2>
                <p class="master-description">{ this.state.preset.description }<br/>{ this.state.preset.autor }</p>
                <div id="player-menu">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-backward"></i></button>
                    <button class="btn btn-primary-outline" type="submit"><i class="fa fa-play"></i></button>
                    <button class="btn btn-primary" type="submit"><i class="fa fa-forward"></i></button>
                </div>
            </div>
        )
    }
}

class App extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div id="App">
                <Player />
            </div>
        )
    }
}

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('app')
);